#include <stdio.h>
void pattern(int n);


int main()
{
    int num;
    
    printf("Enter a number:");
    scanf("%d",&num);
    pattern(num);

    return 0;
}

void pattern(int n){
    
    if(n != 0){
        pattern(n-1);
        for(int i=n;i>=1;i--)
            printf("%d",i);
        printf("\n");
    }
}