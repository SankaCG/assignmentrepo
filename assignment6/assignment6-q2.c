#include <stdio.h>
int fibonacciSeq(int n);

int main(){

	int n;
	printf("Enter a number:");
	scanf("%d",&n);

	for(int i=0;i<=n;i++)
		printf("%d\n",fibonacciSeq(i));

	return 0;

}

int fibonacciSeq(int n){

	if(n==1)
		return 1;
	else if(n==0)
		return 0;
	else
		return fibonacciSeq(n-1)+fibonacciSeq(n-2);

}
