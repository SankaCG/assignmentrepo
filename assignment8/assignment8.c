#include <stdio.h>
#include <string.h>

struct Student {

	char fname[30];
	char subject[30];
	int mark;	

};

int main() {

	struct Student students[5];

	for(int i=0;i<5;i++){
		
		printf("Enter student first name:");
		scanf("%s",students[i].fname);
		printf("Enter subject name:");
		scanf("%s",students[i].subject);
		printf("Enter subject mark:");
		scanf("%d",&students[i].mark);
		printf("\n");
	}

	for(int i=0;i<5;i++){
	
		printf("Student Name:");
		puts(students[i].fname);
		printf("Subject:");
		puts(students[i].subject);
		printf("Marks:%d\n\n",students[i].mark);
	}

	return 0;
}
