#include <stdio.h>

int main(){

	int size;
	int sum=0;

	printf("Enter the size of the two matrices:");
	scanf("%d",&size);

	int matone[size][size];
	int mattwo[size][size];

	printf("------MATRIX ONE------\n");
	for (int i=0;i<size;i++){
		printf("Enter values for row %d:\n",i+1);
		for(int j=0;j<size;j++)
			scanf("%d",&matone[i][j]);
	}

	printf("------MATRIX TWO------\n");
	for (int i=0;i<size;i++){
		printf("Enter values for row %d:\n",i+1);
		for(int j=0;j<size;j++)
			scanf("%d",&mattwo[i][j]);
	}

	printf("------ADDED MATRIX------\n");
	for (int i=0;i<size;i++){
		for (int j=0;j<size;j++)
			printf("%d ",(matone[i][j]+mattwo[i][j]));
		printf("\n");
	}

	printf("------MULTIPLIED MATRIX------\n");
	for (int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			for(int k=0;k<size;k++)
				sum=sum+(matone[i][k]*mattwo[k][j]);
		printf("%d ",sum);
		sum=0;
		}
		printf("\n");
	}

	return 0;
}
