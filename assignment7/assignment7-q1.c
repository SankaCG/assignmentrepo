#include <stdio.h>
#include <string.h>

int main() {

	char word[100];

	printf("Enter the string:");
	fgets(word,sizeof(word),stdin);

	printf("\nThe Word Reversed:\n");
	for(int i=(strlen(word)-1);i>=0;i--)
		printf("%c",word[i]);
	printf("\n");

	return 0;

}
