#include <stdio.h>
#include <string.h>

int main(){

	char sent[100];
	char check;
	int i;
	int count=0;
	printf("Enter the string:");
	fgets(sent,sizeof(sent),stdin);

	printf("Enter the character:");
	scanf("%c",&check);

	for(i=0;i<strlen(sent);i++){
		if(sent[i]==check)
			count++;
	}

	printf("Number of same characters:%d\n",count);

	return 0;
}
